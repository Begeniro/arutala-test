using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleRotation : MonoBehaviour
{
    [SerializeField]
    private float rotationSpeed;
    private float rotSpeedVal;

    public void DisableAnimator()
    {
        GetComponent<Animator>().enabled = false;
    }

    private void Start()
    {
        rotSpeedVal = rotationSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            return;
        }
        else
        {
            transform.Rotate(new Vector3(0, Time.deltaTime * rotationSpeed, 0));
        }
        
    }
}
