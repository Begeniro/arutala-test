using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinchZoom : MonoBehaviour
{
    private float initDistance;
    private Vector3 initScale;

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 2)
        {
            var touchZero = Input.GetTouch(0);
            var touchOne = Input.GetTouch(1);

            if (touchZero.phase == TouchPhase.Ended || touchZero.phase == TouchPhase.Canceled ||
                touchOne.phase == TouchPhase.Ended || touchOne.phase == TouchPhase.Canceled)
            {
                return;
            }

            if (touchZero.phase == TouchPhase.Began || touchOne.phase == TouchPhase.Began)
            {
                initDistance = Vector2.Distance(touchZero.position, touchOne.position);
                initScale = transform.localScale;
                Debug.Log(initDistance);
            }
            else
            {          
                var currentDistance = Vector2.Distance(touchZero.position, touchOne.position);
                if (Mathf.Approximately(initDistance, 0))
                {
                    return;
                }
                var factor = currentDistance / initDistance;
                Debug.Log(gameObject.name);

                transform.localScale = initScale * factor;
            }
        }
    }
}
