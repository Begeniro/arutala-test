using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeRotation : MonoBehaviour
{
    private Touch touch;
    private Vector2 touchPosition;
    private Quaternion rotationY;

    [SerializeField]
    private float rotateSpeed;
    private IdleRotation idleRotation;

    private void Start()
    {
        idleRotation = GetComponent<IdleRotation>();
    }
    void Update()
    {
        if(Input.touchCount == 1)
        {         
            touch = Input.GetTouch(0);
            if(touch.phase == TouchPhase.Moved)
            {             
                rotationY = Quaternion.Euler(0f, -touch.deltaPosition.x * rotateSpeed, 0f);
                transform.rotation = transform.rotation * rotationY;
            }
        }
    }
}
