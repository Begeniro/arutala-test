using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;

public class ScreenshotHandler : MonoBehaviour
{
    [SerializeField]
    private GameObject previewPanel;
    [SerializeField]
    private Image previewImage;
    [SerializeField]
    private GameObject captureButton;

    private string imageName;
    private string path;
    private string finalPath;

    public void Capture()
    {
        StartCoroutine(IECapture());
    }

    private IEnumerator IECapture()
    {
        captureButton.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        imageName = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        path = Application.persistentDataPath + Path.DirectorySeparatorChar + imageName + ".png";

        ScreenCapture.CaptureScreenshot(imageName + ".png");
        Debug.Log(path);
        while(!File.Exists(path))
        {
            yield return null;
        }
        string homeRoot = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
        if (!Directory.Exists("/storage/emulated/0/Pictures"))
        {
            Directory.CreateDirectory("/storage/emulated/0/Pictures");
        }
        if (!Directory.Exists("/storage/emulated/0/Pictures/Arutala"))
        {
            Directory.CreateDirectory("/storage/emulated/0/Pictures/Arutala");
        }

        finalPath = "/storage/emulated/0/Pictures/Arutala/" + imageName + ".png"; // could be anything
        System.IO.File.Move(path, finalPath);
        using (AndroidJavaClass jcUnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        using (AndroidJavaObject joActivity = jcUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
        using (AndroidJavaObject joContext = joActivity.Call<AndroidJavaObject>("getApplicationContext"))
        using (AndroidJavaClass jcMediaScannerConnection = new AndroidJavaClass("android.media.MediaScannerConnection"))
        using (AndroidJavaClass jcEnvironment = new AndroidJavaClass("android.os.Environment"))
        using (AndroidJavaObject joExDir = jcEnvironment.CallStatic<AndroidJavaObject>("getExternalStorageDirectory"))
        {
            jcMediaScannerConnection.CallStatic("scanFile", joContext, new string[] { finalPath }, null, null);
        }

        previewImage.sprite = TextureToSprite(LoadPNG(finalPath));
        previewPanel.SetActive(true);
    }

    private Texture2D LoadPNG(string filePath)
    {
        Texture2D texture = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            texture = new Texture2D(2, 2, TextureFormat.BGRA32, false);
            texture.LoadImage(fileData);
        }
        return texture;
    }
    public Sprite TextureToSprite(Texture2D texture)
    {
        return Sprite.Create(texture, new Rect(0f, 0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 50f, 0, SpriteMeshType.FullRect);
    }

    public void SharePicture()
    {
        new NativeShare().AddFile(finalPath).Share();
    }
}
